using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.SQLite;
using System.Linq;
using System.Threading;
using System.Diagnostics;

namespace SixDegrees
{
	public class main
	{
		//insertion point
		public static int Main (string[] args)
		{
			Console.Write("Welcome to SixDegrees. Please enter wikipedia pages as exact titles or pageids.\n\n");

			bool again = false;
			object source, destination;

			do
			{
                //prompt user for departure article
				Console.Write("Departure article:   ");
				source = Console.ReadLine();

                //prompt user for destination article
				Console.Write("Destination article: ");
				destination = Console.ReadLine();

				int temp;

                //determine if the inputs are pageids or titles
				if (Int32.TryParse((string)source, out temp)) source = temp;
				if (Int32.TryParse((string)destination, out temp)) destination = temp;

                
				using (ConnectionSearch cs = new ConnectionSearch())
				{
					try
					{
                        //attempt to find a connection
						List<Article> connection = cs.FindConnection(source, destination);
                        
						if (connection != null)
						{
							Console.WriteLine();
							Console.Write(connection[0].Title);
							for (int i = 1; i < connection.Count; ++i)
							{
								Console.Write(" -> " + connection[i].Title);
							}
							Console.Write("\n\n");
						}
					}
					catch (ConnectionSearchException e)
					{ Console.WriteLine(e.Message); }
					catch (Exception e)
					{
						Console.Write("Could not determine link");
#if DEBUG
						Console.WriteLine(e.Message);
#endif
					}
				}

                //prompt the user for another search
				Console.Write("\nExit? (Y/N) ");
				char response = Console.ReadKey().KeyChar;
				Console.WriteLine ();

				if ((response == 'Y') || (response == 'y'))
					again = false;
				else
					again = true;

			} while (again);

			/*
			//for debugging
			using (ConnectionSearch cs = new ConnectionSearch())
			{
				try
				{
					connection = cs.FindConnection("Shark Peak", "BC Lions");
				}
				catch
				{
#if DEBUG
					System.Diagnostics.Debugger.Break();
#endif
				}
				finally
				{

				}
			}
			*/
			
			return 0;
		}
	}
}

