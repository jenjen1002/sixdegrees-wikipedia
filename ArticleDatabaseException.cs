﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	class ArticleDatabaseException : Exception
	{
		public ArticleDatabaseException() : base()
		{
		}

		public ArticleDatabaseException(string message) : base(message)
		{
		}
	}
}
