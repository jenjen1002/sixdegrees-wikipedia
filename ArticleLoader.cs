﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	/// <summary>
	/// Base class that defines the different types of information
	/// inherited classes must be capable of loading.
	/// </summary>
	abstract class ArticleLoader
	{
		public readonly Dictionary<Article.InfoType, Func<Article, bool>> Load;

		public ArticleLoader()
		{
			Load = new Dictionary<Article.InfoType, Func<Article, bool>>();
			Load.Add(Article.InfoType.Links, LoadLinks);
			Load.Add(Article.InfoType.Backlinks, LoadBacklinks);
		}

		abstract public bool GetArticle(object article_identifier, out Article new_article);
		abstract public bool LoadLinks(Article article);
		abstract public bool LoadBacklinks(Article article);

	}
}
