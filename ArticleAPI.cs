using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace SixDegrees
{
	class ArticleAPI : ArticleLoader
	{
		/// <summary>base request string. This portion of a request never changes. </summary>
		const string base_string = "http://en.wikipedia.org/w/api.php?format=json&action=query";
		/// <summary>pattern of the date string returned by the wikipedia api. </summary>
		const string date_pattern = "yyyy-MM-ddTHH:mm:ssZ";

		/// <summary>
		/// Retrieves an Article object representing the
		/// article identified by a given pageid or title.
		/// </summary>
		/// <param name="article_identifier">Pageid or title of a wikipedia article.</param>
		/// <param name="new_article">Article object to be returned</param>
		/// <returns>True if successful, false if not.</returns>
		override public bool GetArticle(object article_identifier, out Article new_article)
		{
			new_article = null;
			//make sure the identifier is an int or string
			if (article_identifier is string)
				article_identifier = @"&titles=" + article_identifier;
			else if (article_identifier is int)
				article_identifier = @"&pageids=" + article_identifier.ToString();
			else
				//invalid identifier type
				return false;

			//construct the api request string
			string request_string = base_string + article_identifier + "&prop=info&redirects";

			//retrieve and parse the api response
			Dictionary<string, object> basic_info = null;
			try {
				basic_info = ResponseParser.Parse(retrieve_api_response(request_string));
			} catch (Exception e) {
#if DEBUG
				Console.WriteLine("Unable to load article: " + article_identifier.ToString());
				Console.WriteLine(e.Message);
#endif
				return false;
			}
			basic_info = (Dictionary<string, object>)((Dictionary<string, object>)(((Dictionary<string, object>)basic_info["query"])["pages"])).Values.First();

			//a response does not guarantee a page's existance, if the dictionary contains a "missing" key then the page was not found
			if (basic_info.ContainsKey("missing")) return false;
			//we only care about the main articles in the default namespace
			if ((int)basic_info["ns"] != 0) return false;

			new_article = new Article((int)basic_info["pageid"], (string)basic_info["title"]);
			return true;
		}

		/// <summary>
		/// Retrieves the last time the article with the given pageid was revised/touched.
		/// </summary>
		/// <param name="article">Article object to retrieve the touched date of.</param>
		/// <returns>Date and Time the article was last revised.</returns>
		public DateTime GetLastTouched(Article article)
		{
			//construct the api request string
			string request_string = base_string + "&pageids=" + article.PageID.ToString() + "&prop=info&redirects";

			//retrieve and parse the api response
			Dictionary<string, object> basic_info = null;
			try {
				basic_info = ResponseParser.Parse(retrieve_api_response(request_string));
			} catch (Exception e) {
#if DEBUG
				Console.WriteLine("Unable to load last touched dates for article: " + article.PageID.ToString());
				Console.WriteLine(e.Message);
#endif
				//DateTime is not nullable, so return the min value to indicate an error
				return DateTime.MinValue;
			}

			//navigate to the dictionary object containing the information
			basic_info = (Dictionary<string, object>)((Dictionary<string, object>)(((Dictionary<string, object>)basic_info["query"])["pages"])).Values.First();

			//a response does not guarantee a page's existance,
			//if the dictionary contains a "missing" key then the page was not found
			if (basic_info.ContainsKey("missing"))
				return DateTime.MinValue;

			return DateTime.ParseExact((string)basic_info["touched"], date_pattern, System.Globalization.CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Retrieves the links contained in a wikipedia page with a given pageid.
		/// </summary>
		/// <param name="article">Links contained in this article object will be retrieved</param>
		/// <returns>True if successful, false if not.</returns>
		override public bool LoadLinks(Article article)
		{
			article.Links = null;

			//the resulting list of pageids
			List<int> links = new List<int>();

			//construct the api request string
			string request_string = base_string + "&pageids=" + article.PageID.ToString() + "&generator=links&gpllimit=max&gplnamespace=0&redirects";
			string continue_string = "", lastc_string = "";
            

			//wikipedia's api only returns a maximum of 500 results at a time, therefore
			//additional queries may be needed to complete the result set
			bool is_more = false;

			
			Dictionary<string, object> parsed_info, pages, temp;

			do
			{
				//request and parse the links in a page
				try
				{
					parsed_info = ResponseParser.Parse(retrieve_api_response(request_string + continue_string));
				} catch (Exception e) {
#if DEBUG
					Console.WriteLine("Could not retrieve link information for article: " + article.PageID.ToString());
					Console.WriteLine(e.Message);
#endif
					return false;
				}

				//check for a "query" key, if no key is found the page doesn't exist
				if (!parsed_info.ContainsKey("query")) return false;

				//navigate to the list of results
				pages = (Dictionary<string, object>)((Dictionary<string, object>)parsed_info["query"])["pages"];

				//add each result to the list of pageids
				foreach (object value in pages)
				{
					temp = (Dictionary<string, object>)((KeyValuePair<string, object>)value).Value;
					
					//check if the link leads to a non-existant page
					if (temp.ContainsKey("missing")) continue;

					links.Add((int)temp["pageid"]);
				}

				//check for additional links
				is_more = parsed_info.ContainsKey("query-continue");
				if (is_more)
				{
                    //used to compare the continue parameters to avoid looping queries
                    lastc_string = continue_string;

					//wikipedia requires an additional parameter for continuing a query
					//when the number of results exceeds 500
					continue_string = "&gplcontinue=";
					continue_string +=
						(string)((Dictionary<string, object>)((Dictionary<string, object>)parsed_info["query-continue"])["links"])["gplcontinue"];

                    //if the new continue parameter is equal to the old value there is an endless query loop
                    if (lastc_string.Equals(continue_string))
                    {
                        is_more = false;
#if DEBUG
                        Console.WriteLine("Warning, endless query loop detected on page : " + article.PageID + ". Information may be missing as a result.");
#endif
                    }
				}

			} while (is_more);

			article.Links = links;
			return true;
		}

		/// <summary>
		/// Retrieves the backlinks found on a wikipedia page of the given Article object.
		/// </summary>
		/// <param name="article">Links contained in this article object will be retrieved.</param>
		/// <returns>True if successful, false if not.</returns>
		override public bool LoadBacklinks(Article article)
		{
			article.BackLinks = null;
			//list of pageids to be returned
			List<int> links = new List<int>();

			string request_string = base_string + "&blpageid=" + article.PageID.ToString() + "&list=backlinks&bllimit=max&blfilterredir=nonredirects&blnamespace=0";
			string continue_string = "", lastc_string = "";

			//flag for incomplete queries
			bool is_more = false;

			Dictionary<string, object> parsed_info;
			List<object> pages;

			do
			{
				//request and parse the backlinks in a page
				try
				{
					parsed_info = ResponseParser.Parse(retrieve_api_response(request_string + continue_string));
				}
				catch (Exception e)
				{
#if DEBUG
					Console.WriteLine("Could not retrieve backlink information for article: " + article.PageID.ToString());
					Console.WriteLine(e.Message);
#endif
					return false;
				}

				//check for a "query" key, if no key is found the page doesn't exist
				if (!parsed_info.ContainsKey("query")) return false;

				//navigate to the list of results
				pages = (List<object>)((Dictionary<string, object>)parsed_info["query"])["backlinks"];

				foreach (object value in pages)
					links.Add((int)((Dictionary<string, object>)value)["pageid"]);

				//check for additional links
				is_more = parsed_info.ContainsKey("query-continue");
				if (is_more)
                {
                    //used to compare the continue parameters to avoid looping queries
                    lastc_string = continue_string;

                    //wikipedia requires an additional parameter for continuing a query
                    //when the number of results exceeds 500
					continue_string = "&blcontinue=";
					continue_string += (string)((Dictionary<string, object>)((Dictionary<string, object>)parsed_info["query-continue"])["backlinks"])["blcontinue"];

                    //if the new continue parameter is equal to the old value there is an endless query loop
                    if (lastc_string.Equals(continue_string))
                    {
                        is_more = false;
#if DEBUG
                        Console.WriteLine("Warning, endless query loop detected on page : " + article.PageID + ". Information may be missing as a result.");
#endif
                    }
				}

			} while (is_more);

			article.BackLinks = links;
			return true;
		}

		//===Private Utility Functions Follow===

		/// <summary>
		/// Attempts to retreive a response from wikipedia's API.
		/// If unsuccessful the thread will sleep for .5 seconds and
		/// try again. A total of 10 attempts will be made.
		/// </summary>
		/// <param name="request_string">Formatted URL of the appropriate wikipedia api query.</param>
		/// <returns>Json formatted string of the response or null if the request failed.</returns>
		static string retrieve_api_response(string request_string)
		{
			string returned_data = "";
			bool success = false;
			int failure_count = 0;
			HttpStatusCode status_code = 0;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(base_string + request_string);

			while (failure_count < 10 && !success)
			{
				try
				{
					//contact the wiki api for a response
					using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
					{
						//check the status code and read the received stream if the code is 'OK'
						status_code = response.StatusCode;
						if (status_code == HttpStatusCode.OK)
						{
							StreamReader readStream = null;
							try
							{
								//wrap in a try block so Close() can be called regardless of outcome
								using (Stream receiveStream = response.GetResponseStream())
								{
									if (response.CharacterSet == null)
										readStream = new StreamReader(receiveStream);
									else
										readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
									returned_data = readStream.ReadToEnd();
									success = true;
									readStream.Close();
								}
							}
							catch (Exception)	{ throw; }
							finally				{ readStream.Close(); }
						}
						else
							throw new ArticleAPIException(String.Format("Http Status Code: '{0}'.", status_code.ToString()));
					}
				}
				catch (Exception e)
				{
					//add to failure count
					++failure_count;
					if (failure_count >= 10) throw new ArticleAPIException("Error retrieving API response. Last known error: " + e.Message);
					//wait half a second to try again
					System.Threading.Thread.Sleep(500);
				}
			}

			return returned_data;
		}

		/* ==================================================================
		 * The following array & function was initially used in part
		 * to extract key words in the article's body, but due to time
		 * limitations the weighted decision making was removed and this
		 * method is no longer referenced in the project.
		 * 
		 */

		/// <summary>Any information found after these headings is unwanted. </summary>
		static readonly string[] article_cut_offs = new string[] {	"==Footnotes==",
																	"==References==",
																	"===Sources===",
																	"==External links==",
																	"==See also=="};

		/// <summary>
		/// Strips the wikimarkup formatting from a given string
		/// to produce a mostly text-only string.
		/// </summary>
		/// <param name="source">Markup text to be stripped.</param>
		/// <returns>The 'source' string with most wikimarkup formatting removed.</returns>
		static string strip_markup(String source)
		{
			int j = 0;
			int depth = 0;

			//dispose of unneeded tail sections
			int current_end = source.Length;
			int possible_end = current_end;
			foreach (string cut_off in article_cut_offs)
			{
				possible_end = source.IndexOf(cut_off);
				if (possible_end < current_end && possible_end != -1)
				{
					current_end = possible_end;
				}
			}
			if(current_end < source.Length)
				source = source.Remove(current_end);

			//remove html-like tags
			source = Regex.Replace(source, @"<!--[^\-]-->", "");
			source = Regex.Replace(source, @"<ref[^<]+</ref>", "");
			source = Regex.Replace(source, @"<ref[^>]+>", "");
			source = Regex.Replace(source, @"<sup>|</sup>", "");

			for (int i = 0; i < source.Length; ++i)
			{
				switch (source[i])
				{
					//remove redirects, functions, page widgets and info boxes
					case '{':
						j = i + 1;
						depth = 1;
						try
						{
							while (depth > 0)
							{
								if (source[j] == '{') ++depth;
								else if (source[j] == '}') --depth;
								++j;
							}
						}
						catch (IndexOutOfRangeException)
						{
							//'{' with no closing '}' causing j to go out of range.
							//set j to one more than so the offending character can be removed
							j = i + 1;
						}
						source = source.Remove(i, j - i);
						--i;
						break;
					//remove '<>' tags
					case '<':
						j = i + 1;
						depth = 1;
						try
						{
							while (depth > 0)
							{
								if (source[j] == '<') ++depth;
								else if (source[j] == '>') --depth;
								++j;
							}
						}
						catch(IndexOutOfRangeException)
						{
							//'<' with no closing '>' causing j to go out of range.
							//set j to one more than i so the offending character can be removed
							j = i + 1;
						}
						source = source.Remove(i, j - i);
						--i;
						break;
					//skip over and preserve links and link aliases
					case '[':
						j = i + 1;
						depth = 1;
						try
						{
							while (depth > 0)
							{
								if (source[j] == '[') ++depth;
								else if (source[j] == ']') --depth;
								++j;
							}
						}
						catch (IndexOutOfRangeException)
						{
							//'[' with no closing ']' causing j to go out of range.
							//set j to one more than i to ignore it
							j = i + 1;
						}
						i = j;
						break;
					default:
						break;
				};
			}

			//remove newlines
			source = Regex.Replace(source, @"\\n", " ");
			//remove links but keep their aliases
			source = Regex.Replace(source, @"\[\[[^\]]*\|", "[[");
			source = Regex.Replace(source, @"(\]\])|(\[\[)", "");
			//remove headings
			source = Regex.Replace(source, @"===[^=]*===", "");
			source = Regex.Replace(source, @"==[^=]*==", "");
			source = Regex.Replace(source, @"=[^=]*=", "");
			//remove non-breaking spaces
			source = Regex.Replace(source, @"&nbsp;", " ");

			//remove bold & italic tags
			source = Regex.Replace(source, "('{3})", "");
			source = Regex.Replace(source, "('{2})", "");

			return source;
		}

	}
}

