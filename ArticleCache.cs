﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	class ArticleCache : ArticleLoader
	{
		/// <summary> Cached articles.</summary>
		Dictionary<object, Article> cache = new Dictionary<object,Article>();
		/// <summary>Used to ensure thread safety.</summary>
		ReadWriteLock cache_lock = new ReadWriteLock();

		/// <summary>
		/// Returns the main data container of the ArticleCache
		/// </summary>
		public Dictionary<object, Article> Cache
		{ get { return cache; } }

		/// <summary>
		/// Retrieves an Article object representing the
		/// article identified by a given pageid or title.
		/// </summary>
		/// <param name="article_identifier">Pageid or title of a wikipedia article.</param>
		/// <param name="new_article">Article object to be returned</param>
		/// <returns>True if successful, false if not.</returns>
		override public bool GetArticle(object article_identifier, out Article new_article)
		{
			new_article = null;
			//notify a read has started on the cache
			cache_lock.StartRead();
			try
			{
				Article cached_article;
				//does the article exist in the cache?
				if (cache.TryGetValue(article_identifier, out cached_article))
					new_article = cached_article;
				else
					return false;

				return true;
			}
			catch { return false; }
			finally { cache_lock.EndRead(); }
		}

		/// <summary>
		/// Retrieves the links contained in a wikipedia page with a given pageid.
		/// </summary>
		/// <param name="article">Links contained in this article object will be retrieved</param>
		/// <returns>True if successful, false if not.</returns>
		public override bool LoadLinks(Article article)
		{
			//notify a read has started on the cache
			cache_lock.StartRead();
			try
			{
				Article cached_article;
				//does the article exist in the cache?
				if (cache.TryGetValue(article.PageID, out cached_article))
				{
					//if it does, does it contain the necessary information?
					if (cached_article.Links != null)
					{
						article.Links = cached_article.Links;
						return true;
					}
				}

				return false;
			}
			catch { return false; }
			//notify we the thread is no longer reading from the cache
			finally { cache_lock.EndRead(); }
		}

		/// <summary>
		/// Retrieves the backlinks found on a wikipedia page of the given Article object.
		/// </summary>
		/// <param name="article">Links contained in this article object will be retrieved.</param>
		/// <returns>True if successful, false if not.</returns>
		public override bool LoadBacklinks(Article article)
		{
			//notify a read has started on the cache
			cache_lock.StartRead();
			try
			{
                Article cached_article;
				//does the article exist in the cache?
				if (cache.TryGetValue(article.PageID, out cached_article))
				{
					//if it does, does it contain the necessary information?
					if (cached_article.BackLinks != null)
					{
						article.BackLinks = cached_article.BackLinks;
						return true;
					}
				}

				return false;
			}
			catch { return false; }
			//notify we the thread is no longer reading from the cache
			finally { cache_lock.EndRead(); }
		}

		/// <summary>
		/// Adds article information to the article cache.
		/// </summary>
		/// <param name="article">Article to add.</param>
		/// <param name="itype">Information the article contains</param>
		public void Add(Article article, Article.InfoType itype)
		{
			//notify the readwritelocker we wish to modify the cache
			cache_lock.StartWrite();
			try
			{
                Article cached_article;
				//does an identical article object already exist in the cache?
                if (cache.TryGetValue(article.PageID, out cached_article))
				{
					//an identical article object exists, modifiy the entry
					//get the old element
					switch (itype)
					{
						case Article.InfoType.Links:
                            cached_article.Links = article.Links;
							break;
						case Article.InfoType.Backlinks:
                            cached_article.BackLinks = article.BackLinks;
							break;
					}
					//tuple are immutable, have to create a new one with the desired information
                    cache[article.PageID] = cached_article;
				}
				else
					//new article, add it as is
					cache.Add(article.PageID, article);
			}
			catch (Exception e)
			{
#if DEBUG
				Console.WriteLine("Could not add article: " + article.PageID + " to cache.");
				Console.WriteLine(e.Message);
#endif
			}
			//ensure the cache is freed for further writes/reads incase of any error
			finally { cache_lock.EndWrite(); }
		}

		public void Clear()
		{
			cache.Clear();
		}
	}
}
