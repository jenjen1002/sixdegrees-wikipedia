﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	class ArticleAPIException : Exception
	{
		public ArticleAPIException() : base()
		{
		}

		public ArticleAPIException(string message) : base(message)
		{
		}

	}
}
