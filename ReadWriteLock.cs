﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	/// <summary>
	/// Used to ensure data management classes maintain integrity and are thread
	/// safe. Allows for concurrent reads and exactly one write at a time.
	/// (PS. A ReaderWriterLock class already exists but this class
	/// was complete before I was aware the ReaderWriterLock exists)
	/// </summary>
	class ReadWriteLock : IDisposable
	{
		/// <summary>Counts the number of threads currently executing a read. </summary>
		int read_count = 0;
		/// <summary>Read lock ensures read_count is incremented/decremented atomically. </summary>
		readonly object read_lock = new object();
		/// <summary>Write lock ensures only one thread may write at a time. </summary>
		readonly object write_lock = new object();

		/// <summary>Blocks write threads from writing when it is not safe to do so. </summary>
		System.Threading.EventWaitHandle safe_to_write = new System.Threading.ManualResetEvent(true);
		/// <summary>Blocks read threads from reading when it is not safe to do so. </summary>
		System.Threading.EventWaitHandle safe_to_read = new System.Threading.ManualResetEvent(true);

		//ensures atomic console output, used for debugging since my
		//version of visual c# does not include a 'threads' window
		//object console_lock = new object();

		/// <summary>
		/// Notifies the readwritelock object that a thread wishes to read data from resource.
		/// To be called before the data container is accessed.
		/// </summary>
		public void StartRead()
		{
			//give priority to writes
			safe_to_read.WaitOne();
			
			//wait for current writes to complete
			lock (write_lock)
			{
				//disallow writes while being read
				safe_to_write.Reset();

				lock (read_lock)
				{
					//lock (console_lock) { Console.WriteLine("sr1 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId); }
					++read_count;
					//lock (console_lock) { Console.WriteLine("sr2 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId); }
				}
			}
		}

		/// <summary>
		/// Notifies the readwritelock object that a thread is finished reading from a resource.
		/// Called after accessing the data container. Must be placed inside of a finally to ensure
		/// the resource is always freed.
		/// </summary>
		public void EndRead()
		{
			lock (read_lock)
			{
				//lock (console_lock)	{Console.WriteLine("er1 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId);}
				--read_count;
				//make it safe to write (remove block on write threads)
				if (read_count == 0)
				{
					//no more reads in progress, it's safe to write
					//lock (console_lock) { Console.WriteLine("safe to write - " + System.Threading.Thread.CurrentThread.ManagedThreadId); }
					safe_to_write.Set();
				}
				//lock (console_lock)	{Console.WriteLine("er2 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId);}
			}
		}

		/// <summary>
		/// Notifies the readwritelock object that a thread would like to modifiy a resource.
		/// Called before accessing the resource.
		/// </summary>
		public void StartWrite()
		{
			
			//ensure only one write occurs at a time
			System.Threading.Monitor.Enter(write_lock);
			//lock (console_lock) { Console.WriteLine("sw1 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId); }
			//disallow reads
			safe_to_read.Reset();
			//wait for reads to complete
			while (read_count != 0)
				safe_to_write.WaitOne();
			//if (read_count != 0) System.Diagnostics.Debugger.Break();
			//disallow multiple writes at a time
			safe_to_write.Reset();
			//lock (console_lock) { Console.WriteLine("sw2 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId); }
		}

		/// <summary>
		/// Notifies the readwritelock object that a thread is no longer modifying a resource.
		/// Called after accessing the resource. Must be placed in a finally block to ensure the
		/// resource is freed.
		/// </summary>
		public void EndWrite()
		{
			//unblock the threads waiting to read
			lock (read_lock)
			{
				//lock (console_lock)	{Console.WriteLine("ew1 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId);}
				safe_to_read.Set();
				//lock (console_lock) { Console.WriteLine("safe to write - " + System.Threading.Thread.CurrentThread.ManagedThreadId); }
				safe_to_write.Set();
				//lock (console_lock)	{Console.WriteLine("ew2 - " + read_count + " - " + System.Threading.Thread.CurrentThread.ManagedThreadId);}
			}
			System.Threading.Monitor.Exit(write_lock);
		}

		/// <summary>Dispose of the EventWaitHandle obejcts. </summary>
		public void Dispose()
		{
			safe_to_read.Dispose();
			safe_to_write.Dispose();
		}

	}
}
