﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	class SearchJournalException : Exception
	{
		public SearchJournalException() : base()
		{
		}

		public SearchJournalException(string message) : base(message)
		{
		}
	}
}
