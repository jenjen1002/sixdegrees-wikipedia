# SixDegrees of Wikipedia

Inspired by the "Six Degrees of Kevin Bacon", this applicaiton determines the relation between two Wikipedia articles by searching the hyperlinks and backlinks in the articles.

## Getting Started

The repository is organized as a Visual Studio project/solution.

## Demonstration

![input](readme_img/demo2.PNG)

![output](readme_img/demo3.png)