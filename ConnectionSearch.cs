﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SixDegrees
{
	/// <summary>
	/// Used to determine the connection from a departure article
	/// to a destination article through links found in the articles.
	/// </summary>
	class ConnectionSearch : IDisposable
	{
		/// <summary>Stores articles in the order they are navigatable. </summary>
		List<Article> connection_path = null;
		Article[] root = null;
		/// <summary>Database wrapper for storing the current search data. </summary>
		SearchJournal journal = null;
		ArticleInfoRetriever retriever = null;


		readonly object return_links_lock = new object();
		readonly object return_weights_lock = new object();

		public Boolean QuickLookup { get; set; }

		public ConnectionSearch(bool quick_lookup = true)
		{
			QuickLookup = quick_lookup;
			retriever = new ArticleInfoRetriever(QuickLookup);
		}

		public List<Article> Connection
		{
			get{return connection_path;}
		}

		public bool Success
		{
			get{ return connection_path != null; }
		}

		/// <summary>
		/// Attempts to determine the connection between two wikipedia articles.
		/// </summary>
		/// <param name="departure_article">Pageid or title of the departure article.</param>
		/// <param name="destination_article">Pageid or title of the destination article.</param>
		/// <returns>A list of linked articles or null if no connection exists</returns>
		public List<Article> FindConnection(object departure_article, object destination_article)
		{
			//check for a search already in progress
			if (root != null)
				throw new ConnectionSearchException("This object is already executing a search.");
			root = new Article[2];

			try
			{
				journal = new SearchJournal();
				Article temp = null;
				connection_path = null;
				bool no_path = false;

				if (!retriever.LoadArticle(departure_article, out temp))
					throw new ConnectionSearchException("Could not load departure article.");
				root[0] = temp;

				if (!retriever.LoadArticle(destination_article, out temp))
					throw new ConnectionSearchException("Could not load destination article.");
				root[1] = temp;

				Console.WriteLine(string.Format("Attempting to find links from '{0}' to '{1}'",
					root[0].Title, root[1].Title));

				journal.InsertRoot(root[0].PageID, SearchJournal.Side.Left);
				journal.InsertRoot(root[1].PageID, SearchJournal.Side.Right);

				List<int> page_list = null;
				List<int> link_list = null;

				int step = 0;
				SearchJournal.Side side = SearchJournal.Side.Right;

				//main logic loop
				while (!journal.PathExists() && !no_path)
				{
					//alternate sides
					if (side == SearchJournal.Side.Left) side = SearchJournal.Side.Right;
					else side = SearchJournal.Side.Left;

					++step;
					Console.WriteLine("Step " + step + " " + SearchJournal.side_to_string[side]);
					page_list = journal.GetUnchecked(side);
					if (page_list.Count == 0)
						//indicates a dead end if no pages are returned unchecked
						no_path = true;
					else
						AddLinks(page_list, side);
				}

				//check if there was a dead end
				if (no_path)
				{
					Console.WriteLine("Could not determine a link.");
					return null;
				}

				//retrieve the connection path
				link_list = journal.GetPath();

				//double check that the connection path exists
				if (link_list == null)
				{
					Console.WriteLine("Could not determine a link.");
					return null;
				}

				Console.WriteLine("Article links found.");

				List<Article> return_list = new List<Article>();

				foreach (int id in link_list)
				{
					if (retriever.LoadArticle(id, out temp))
						return_list.Add(temp);
				}

				connection_path = return_list;
				return return_list;
			}
			catch (Exception e)
			{
#if DEBUG
				Console.WriteLine("Error determining connection.");
				Console.WriteLine(e.Message);
#endif
				return null;
			}
			finally
			{
				root = null;
				journal.Dispose();
				journal = null;
			}
		}

		/// <summary>
		/// Finds the links contained in all the articles corresponding to
		/// the pageids contained in a list.
		/// </summary>
		/// <param name="articles">List of pageids to retrieve links for</param>
		/// <param name="side">The side of the search the links required for.</param>
		void AddLinks(List<int> articles, SearchJournal.Side side)
		{
			//list of returned links to be added to the current search journal
			List<Tuple<int, List<int>>> link_list = new List<Tuple<int, List<int>>>();
			//used to ensure link_list is thread safe.
			object list_lock = new object();

			int completed = 0;
			object completed_lock = new object();
			double pc = 0;
			int total = articles.Count;
			bool finished = false;

			//producer consumer setup
			using (EventWaitHandle consumer_wait = new AutoResetEvent(false))
			{
				
				//used to end the parallel loop early if a solution is found
				CancellationTokenSource c_token = new CancellationTokenSource();

				//producer threads are responsible for populating the 'link_list'
				Thread producer = new Thread(() =>
				{
					//do we need links or backlinks?
					Article.InfoType itype = SearchJournal.side_to_itype[side];
					//suppress journal output for now
					journal.Verbose = false;


					ParallelOptions p_options = new ParallelOptions();
					p_options.CancellationToken = c_token.Token;
					

					//we need to load link information for many articles which may require
					//many api requests. Many threads are used to ensure the time between
					//an api request and response are utilized to start information retrieval
					//of other articles.
					try
					{
						Parallel.ForEach(articles, p_options, article =>
						{
							Article a;

							//check if a solution was found early
							p_options.CancellationToken.ThrowIfCancellationRequested();

							try
							{
								//load the required article and link information
								if (retriever.LoadArticle(article, out a))
									if (retriever.LoadInfo(a, itype))
									{
										//add the information to 'link_list'
										lock (list_lock)
										{
											link_list.Add(new Tuple<int, List<int>>
											(a.PageID, a.GetLinks((Article.LinkType)(int)itype)));
											//notify the consumer thread to commit the
											//current link_list to the search journal
											if (link_list.Count >= 10) consumer_wait.Set();
										}
									}
							}
							catch (Exception e)
							{
#if DEBUG
								Console.WriteLine("Failed to retrieve link information for article: " +
								article.ToString() + e.Message + "\n");
#endif
							}
							finally
							{
								//output the current compelted percent
								lock (completed_lock)
								{
									++completed;
									pc = ((double)completed / (double)total) * (double)100;
									Console.Write("\r" + pc.ToString("000.00") + " %");
								}
							}
						});

						Console.Write("\r");
						finished = true;
						journal.Verbose = false;
						consumer_wait.Set();
					}
					catch (OperationCanceledException)
					{}
				});

				producer.Start();

				while (!finished || (link_list.Count != 0))
				{
					consumer_wait.WaitOne();
					//empty the link list
					List<Tuple<int, List<int>>> temp = null;
					lock (list_lock)
					{
						temp = link_list;
						link_list = new List<Tuple<int, List<int>>>();
					}
					//commit the current list
					journal.InsertLinks(temp, side);
					//if a solution exists end the parallel loop
					if (journal.PathExists())
					{
						c_token.Cancel();
						break;
					}
				}
				//wait for producer threads to finish
				producer.Join();
				Console.Write("\r");
			}
		}

		public void Dispose()
		{
			retriever.Dispose();
		}
	}
}
