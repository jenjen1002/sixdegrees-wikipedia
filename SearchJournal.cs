﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.IO;
using System.Data;

namespace SixDegrees
{
	class SearchJournal : IDisposable
	{
		const string create_left_table_string =
			"CREATE TABLE IF NOT EXISTS left (pageid INT PRIMARY KEY, parent INT REFERENCES left(pageid), checked INT DEFAULT 0)";
		const string create_right_table_string =
			"CREATE TABLE IF NOT EXISTS right (pageid INT PRIMARY KEY, parent INT REFERENCES right(pageid), checked INT DEFAULT 0)";
		const string path_test_string = 
			"SELECT count(*) FROM left, right WHERE left.pageid == right.pageid;";
		const string path_connection_string = 
			"SELECT left.pageid, left.parent, right.parent FROM left, right WHERE left.pageid == right.pageid;";

		public enum Side { Left = 0, Right = 1};

		public static readonly Dictionary<Side, string> side_to_string;
		public static readonly Dictionary<string, Side> string_to_side;

		static public readonly Dictionary<Side, Article.InfoType> side_to_itype;

		static SearchJournal()
		{
			side_to_string = new Dictionary<Side, string>();
			side_to_string.Add(Side.Left, "Left");
			side_to_string.Add(Side.Right, "Right");
			string_to_side = new Dictionary<string, Side>();
			string_to_side.Add("Left", Side.Left);
			string_to_side.Add("Right", Side.Right);
			side_to_itype = new Dictionary<Side, Article.InfoType>();
			side_to_itype.Add(Side.Left, Article.InfoType.Links);
			side_to_itype.Add(Side.Right, Article.InfoType.Backlinks);
		}

		Guid object_id;
		readonly string cstring;

		public bool Verbose
		{ get; set; }

		public SearchJournal()
		{
			//get a unique object identifier and use it as the journal name
			object_id = Guid.NewGuid();
			cstring = "Data Source=" + object_id.ToString();

			try
			{
				using (SQLiteConnection connection = new SQLiteConnection(cstring))
				{
					//open the database connection
					connection.Open();

					using (SQLiteTransaction transaction = connection.BeginTransaction())
					using (SQLiteCommand command = new SQLiteCommand(connection))
					{
						//set the journal mode to memory to reduce io operations
						//the database has a chance of corruption if the program is
						//unexpectedly halted, however searchjournal databases are
						//temporary and corruption is not a concern
						command.CommandText = "PRAGMA journal_mode = MEMORY;";
						command.ExecuteNonQuery();

						//create the main tables
						command.CommandText = create_left_table_string;
						command.ExecuteNonQuery();
						command.CommandText = create_right_table_string;
						command.ExecuteNonQuery();

						transaction.Commit();
					}
				}
			}
			catch (Exception e)
			{
				throw new SearchJournalException(e.Message);
			}
		}

		
		/// <summary>
		/// Inserts a single article's search info into the journal.
		/// </summary>
		/// <param name="pageid"></param>
		/// <param name="side">Is it a departure or a destination article.</param>
		/// <param name="parentid">A value of zero indicates this row is a root.</param>
		/// <param name="is_checked"></param>
		public void InsertRoot(int pageid, Side side)
		{
			using (SQLiteConnection connection = new SQLiteConnection(cstring))
			{
				connection.Open();
				using (SQLiteCommand command = new SQLiteCommand(connection))
				{
					command.CommandText = "INSERT OR IGNORE INTO " + side_to_string[side] + " (pageid, parent) VALUES (@pageid, @parent);";
					command.Parameters.AddWithValue("@pageid", pageid);
					command.Parameters.AddWithValue("@parent", "0");

					command.ExecuteNonQuery();
				}
				connection.Close();
			}
		}

		/// <summary>
		/// Insert a list of links into the search journal.
		/// </summary>
		/// <param name="parent">Parent of the inserted links.</param>
		/// <param name="side">Which side of the table to insert on.</param>
		/// <param name="pageids">list of page IDs to be inserted.</param>
		public void InsertLinks(List<Tuple<int, List<int>>> links, Side side)
		{
			if (links.Count > 0)
			{
				if (Verbose) Console.WriteLine("Updating search journal");

				//open the database connection
				using (SQLiteConnection connection = new SQLiteConnection(cstring))
				{
					connection.Open();

					//begin a new transaction
					using (SQLiteTransaction transaction = connection.BeginTransaction())
					using (SQLiteCommand insert_command = new SQLiteCommand(connection))
					using (SQLiteCommand update_command = new SQLiteCommand(connection))
					using (SQLiteCommand fk_on = new SQLiteCommand(connection))
					{
						fk_on.CommandText = "PRAGMA foreign_keys = ON;";
						fk_on.ExecuteNonQuery();

						insert_command.CommandText = "INSERT OR IGNORE INTO " + side_to_string[side] + " (pageid, parent) VALUES (@pageid, @parent);";
						insert_command.Parameters.AddWithValue("@pageid", "");
						insert_command.Parameters.AddWithValue("@parent", "");

						update_command.CommandText = "UPDATE OR IGNORE " + side_to_string[side] + " SET checked = 1 WHERE pageid = @parent";
						update_command.Parameters.Add(insert_command.Parameters["@parent"]);

						int total = links.Count;
						int completed = 0;
						double pc = 0;

						foreach (Tuple<int, List<int>> page in links)
						{
							insert_command.Parameters["@parent"].Value = page.Item1;
							

							foreach (int pid in page.Item2)
							{
								insert_command.Parameters["@pageid"].Value = pid;
								insert_command.ExecuteNonQuery();
							}

							//update the parent row
							update_command.ExecuteNonQuery();

							++completed;
							if (Verbose)
							{
								pc = ((double)completed / (double)total) * (double)100;
								Console.Write("\r" + pc.ToString("000.00") + " %");
							}
						}

						//commit all of the changes
						if(Verbose) Console.Write("\rCommitting transaction...");
						transaction.Commit();
						if(Verbose) Console.Write("\rTransaction complete.    \n");
					}
					connection.Close();
				}
			}
		}

		/// <summary>
		/// Retrieves a list of unchecked pageIDs
		/// </summary>
		/// <returns>A list of page IDs.</returns>
		public List<int> GetUnchecked(Side side)
		{
			List<int> pageids = new List<int>();

			using (SQLiteConnection connection = new SQLiteConnection(cstring))
			{
				connection.Open();
				using (SQLiteCommand command = new SQLiteCommand(connection))
				{
					command.CommandText = "SELECT pageid FROM " + side_to_string[side] + " WHERE checked = 0";

					using (SQLiteDataReader reader = command.ExecuteReader())
					{
						while (reader.Read()) pageids.Add(reader.GetInt32(0));

						reader.Close();
					}
				}
				connection.Close();
			}

			return pageids;
		}

		/// <summary>
		/// Determines if a path is available from departure to destination article.
		/// </summary>
		/// <returns>True if a path is available, false if not.</returns>
		public bool PathExists()
		{
			long return_value = 0;

			//open the database connection
			using (SQLiteConnection connection = new SQLiteConnection(cstring))
			{
				connection.Open();

				using (SQLiteCommand command = new SQLiteCommand(connection))
				{
					command.CommandText = path_test_string;

					//count query will always return a value, no need to check for null
					return_value = (long)command.ExecuteScalar();
				}
				connection.Close();
			}
			return return_value > 0;
		}

		/// <summary>
		/// Retrieves a list of pageIDs corresponding to linked articles that form a connection
		/// between the two root articles.
		/// </summary>
		/// <returns>A list of Page IDs</returns>
		public List<int> GetPath()
		{
			if (!PathExists()) return null;

			List<int> path = new List<int>();
			using (SQLiteConnection connection = new SQLiteConnection(cstring))
			{
				connection.Open();

				int linking_id = 0, left_parent = 0, right_parent = 0;

				using (SQLiteCommand command = new SQLiteCommand(connection))
				{
					command.CommandText = path_connection_string;

					using (SQLiteDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							linking_id = reader.GetInt32(0);
							left_parent = reader.GetInt32(1);
							right_parent = reader.GetInt32(2);
						}

						reader.Close();
					}

					if (linking_id == 0) return null;

					//get all articles to the left of linking article

					command.CommandText = "SELECT parent FROM left WHERE pageid = @pageid";
					command.Parameters.AddWithValue("@pageid", "");

					while (left_parent != 0)
					{
						path.Add(left_parent);

						command.Parameters["@pageid"].Value = left_parent;

						left_parent = (int)command.ExecuteScalar();
					}
					//reverse the order. We want the departure article first
					path.Reverse();

					//add the linking article
					path.Add(linking_id);

					//get all the articles to the right of the linking article

					command.CommandText = "SELECT parent FROM right WHERE pageid = @pageid";
					command.Parameters.AddWithValue("@pageid", "");

					while (right_parent != 0)
					{
						path.Add(right_parent);

						command.Parameters["@pageid"].Value = right_parent;

						right_parent = (int)command.ExecuteScalar();
					}
				}
				connection.Close();
			}
			return path;
		}


		/**Deletes all rows from the current search tables
		 * 
		 **/
		public void Clear()
		{
			//open the database connection
			using (SQLiteConnection connection = new SQLiteConnection(cstring))
			{
				connection.Open();

				//create the main tables
				using (SQLiteCommand command = new SQLiteCommand(connection))
				{
					//execute delete statements
					command.CommandText = "DELETE FROM left;";
					command.ExecuteNonQuery();
					command.CommandText = "DELETE FROM right;";
					command.ExecuteNonQuery();
				}
				connection.Close();
			}
		}

		public virtual void Dispose()
		{
			try
			{
				File.Delete(object_id.ToString());
			}
			catch(Exception)
			{ }
			finally
			{ }
		}


	}
}
