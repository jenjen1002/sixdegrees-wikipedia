﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Text.RegularExpressions;

namespace SixDegrees
{
	/// <summary>
	/// SQLite Database wrapper to simplify storing/retrieving article information.
	/// This class is thread safe and supports multiple concurrent reads, but only
	/// one write. Read threads will be blocked when a write is in progress, and write
	/// threads will be blocked when a read is in progress or another write is in progress/
	/// (sqlite3 is thread safe however the developers advise against it
	/// http://sqlite.org/faq.html#q6, hence me inplementing the readwritelocking mechanism)
	/// </summary>
	class ArticleDatabase : ArticleLoader, IDisposable
	{

		/**********************************************************************
		 *                            ====STATIC====
		 * 
		 *            Static portions of the class are as follows
		 * 
		**********************************************************************/

		//class database string constants
		const string article_table_name = "articles";			//name of the article table
		const string link_table_name = "links";					//name of the link table
        const string backlink_table_name = "backlinks";         //name of the backlink table
		const string date_pattern = "yyyy-MM-ddTHH:mm:ssZ";		//pattern that dates are represented in the database

		//main table creation string
		const string create_article_table = "CREATE TABLE IF NOT EXISTS " + article_table_name +
            " (pageid INT PRIMARY KEY, title TEXT NOT NULL, linksdate TEXT, backlinksdate TEXT)";
        //link table creation string
        const string create_link_table = "CREATE TABLE IF NOT EXISTS " + link_table_name +
            " (sourceid INT NOT NULL, destid INT NOT NULL, PRIMARY KEY(sourceid, destid))";
        //backlink table creation string
        const string create_backlink_table = "CREATE TABLE IF NOT EXISTS " + backlink_table_name +
            " (destid INT NOT NULL, sourceid INT NOT NULL, PRIMARY KEY(destid, sourceid))";

		/// <summary>Start date and time of the current session (when this class was first instantiated).</summary>
		static readonly DateTime session_start;
		/// <summary>List of currently open databases.</summary>
		static List<string> databases_open;


		/// <summary>
		/// Determines the session start time
		/// and creates an empty list of open database files.
		/// </summary>
		static ArticleDatabase()
		{
			session_start = DateTime.UtcNow;
			databases_open = new List<string>();
		}


		/// <summary>Time the current session started.</summary>
		/// <returns>A DateTime object representing when the first ArticleDatabase object was instantiated.</returns>
		public static DateTime SessionStartTime
		{ get { return session_start; } }


		/**********************************************************************
		 *                         ====NON-STATIC====
		 * 
		 *         The non-static portion of the class are as follows
		 * 
		 *********************************************************************/


		/// <summary> Name of the database file to create/connect to.</summary>
		readonly string db_name;
		readonly string connection_string;

		/// <summary>
		/// responsible for allowing numerous reads but only one write at a time.
		/// </summary>
		ReadWriteLock db_lock = new ReadWriteLock();


		/// <summary>
		/// </summary>
		/// <param name="database_name">Name of the article database file.</param>
		public ArticleDatabase(string database_name)
		{
			//lock the database to avoid reads/writes before the object is fully constructed
			lock (db_lock)
			{
				//set object constants
				db_name = database_name;
				connection_string = "Data Source=" + db_name;

				//disallow the new articledatabase if the file is already being used by another object
				lock (databases_open)
				{
					if (databases_open.Contains(db_name))
						throw new ArticleDatabaseException(string.Format("Cannot open database '{0}', it is already opened by another ArticleDatabase object.", db_name));
					else
						databases_open.Add(db_name);
				}

				//ensure there is a master article table
				try
				{
					//open the database connection
					using (SQLiteConnection connection = new SQLiteConnection(connection_string))
					{
						connection.Open();

						//set a command object
						using (SQLiteCommand command = new SQLiteCommand(connection))
						{
							//create the main article table
							command.CommandText = create_article_table;
							command.ExecuteNonQuery();

							//create link table
							command.CommandText = create_link_table;
							command.ExecuteNonQuery();

							//create backlink table
							command.CommandText = create_backlink_table;
							command.ExecuteNonQuery();
						}
						connection.Close();
					}

				}
				catch (Exception e)
				{
					throw new ArticleDatabaseException(e.Message);
				}
			}
		}

		/// <summary>
		/// Releases the database name so it may be used
		/// again with another article database object.
		/// </summary>
		public void Dispose()
		{
			lock (databases_open)	databases_open.Remove(db_name);
            db_lock.Dispose();
		}

		/// <summary>
		/// Commits all articles in the list to the database. However,
		/// Committing only the information indicated by the infoflag field.
		/// This is to avoid unnecessary IO operations.
		/// </summary>
		/// <param name="articles">List of articles and corresponding flags to be commited to the database</param>
		public void Commit(List<Article> articles)
		{
			//notify of a pending write to the database
			db_lock.StartWrite();
			try
			{
#if DEBUG
				Console.WriteLine("Committing article information to database. "
					+ articles.Count + " articles.");
#endif

				using (SQLiteConnection connection = new SQLiteConnection(connection_string))
				{
					connection.Open();
					using (SQLiteTransaction transaction = connection.BeginTransaction())
					{
						try
						{
							//list of commands that will be used in the mass commit
							using (SQLiteCommand update_link_col = connection.CreateCommand())
							using (SQLiteCommand update_backlink_col = connection.CreateCommand())
							using (SQLiteCommand article_insert_command = connection.CreateCommand())
							using (SQLiteCommand link_insert_command = connection.CreateCommand())
                            using (SQLiteCommand backlink_insert_command = connection.CreateCommand())
							using (SQLiteCommand delete_link_command = connection.CreateCommand())
							using (SQLiteCommand delete_backlink_command = connection.CreateCommand())
							{
								//prepare command statements

								//timestamp updates in the main article table
								update_link_col.CommandText = "UPDATE OR IGNORE " + article_table_name +
									" SET linksdate = @value WHERE pageid == @pageid";
								update_backlink_col.CommandText = "UPDATE OR IGNORE " + article_table_name +
                                    " SET backlinksdate = @value WHERE pageid == @pageid";
								update_link_col.Parameters.AddWithValue("@pageid", "");
								update_link_col.Parameters.AddWithValue("@value", "");
								update_backlink_col.Parameters.Add(update_link_col.Parameters["@pageid"]);
								update_backlink_col.Parameters.Add(update_link_col.Parameters["@value"]);

								//insert a new article into the main table
								article_insert_command.CommandText =
									"INSERT OR IGNORE INTO " + article_table_name + " (pageid, title) VALUES (@pageid,@title)";
								article_insert_command.Parameters.AddWithValue("@pageid", "");
								article_insert_command.Parameters.AddWithValue("@title", "");

                                //insert new links
                                link_insert_command.CommandText =
                                    "INSERT OR IGNORE INTO " + link_table_name + " VALUES (@sourceid,@destid)";
                                link_insert_command.Parameters.AddWithValue("@sourceid", "");
                                link_insert_command.Parameters.AddWithValue("@destid", "");

                                //insert new backlinks
                                backlink_insert_command.CommandText =
                                    "INSERT OR IGNORE INTO " + backlink_table_name + " VALUES (@destid,@sourceid)";
                                backlink_insert_command.Parameters.AddWithValue("@sourceid", "");
                                backlink_insert_command.Parameters.AddWithValue("@destid", "");

								//delete old links & backlinks
								delete_link_command.CommandText =
									"DELETE FROM " + link_table_name + " WHERE sourceid == @pageid";
								delete_backlink_command.CommandText =
									"DELETE FROM " + backlink_table_name + " WHERE destid == @pageid";
								delete_link_command.Parameters.AddWithValue("@pageid", "");
								delete_backlink_command.Parameters.AddWithValue("@pageid", "");

								//when are all of the articles being inserted/updated?
								update_link_col.Parameters["@value"].Value = DateTime.UtcNow.ToString(date_pattern);

								//progress trackers
								int total = articles.Count;
								int completed = 0;
								double pc = 0;


                                //the database has grown quite a bit, greatly decreasing insert performace due to the existance
                                //of the backlinks index, drop the backlinks index for now and add it again later
                                //index_command.CommandText = drop_backlink_index;
                                //index_command.ExecuteNonQuery();

								foreach (Article article in articles)
								{
									//insert basic info
									article_insert_command.Parameters["@pageid"].Value = article.PageID;
									article_insert_command.Parameters["@title"].Value = article.Title;
									article_insert_command.ExecuteNonQuery();

									//what row is being updated?
									update_link_col.Parameters["@pageid"].Value = article.PageID;

									//insert links
									if (article.Links != null)
									{
										//remove existing links
										delete_link_command.Parameters["@pageid"].Value = article.PageID;
										delete_link_command.ExecuteNonQuery();

										//enter new links
										link_insert_command.Parameters["@sourceid"].Value = article.PageID;
										foreach (int dest in article.Links)
										{
											link_insert_command.Parameters["@destid"].Value = dest;
											link_insert_command.ExecuteNonQuery();
										}

										//update links entry
										update_link_col.ExecuteNonQuery();
									}

									//insert backlinks
									if (article.BackLinks != null)
									{
										//remove existing backlinks
										delete_backlink_command.Parameters["@pageid"].Value = article.PageID;
										delete_backlink_command.ExecuteNonQuery();

										//enter new backlinks
										backlink_insert_command.Parameters["@destid"].Value = article.PageID;
										foreach (int source in article.BackLinks)
										{
											backlink_insert_command.Parameters["@sourceid"].Value = source;
											backlink_insert_command.ExecuteNonQuery();
										}

										//update backlinks entry
										update_backlink_col.ExecuteNonQuery();
									}

									//update progress
									++completed;
									pc = ((double)completed / (double)total) * (double)100;
									Console.Write("\r" + pc.ToString("000.00") + " %");

								}
							}
#if DEBUG
							Console.Write("\rExecuting transaction.    \n");
#endif
							transaction.Commit();
#if DEBUG
							Console.Write("\rAll " + articles.Count + " articles committed successfully.\n");
#endif
						}
						catch (Exception e)
						{
#if DEBUG
							Console.Write("\nError committing articles to database, attempting to rollback transaction.\n");
#endif
							transaction.Rollback();
							throw e;
						}
					}
				}
			}
			//free the database for later read/writes
			finally
			{
				db_lock.EndWrite();
			}
		}



		/// <summary>
		/// Returns an article object matching the given identifier, either pageid or title.
		/// </summary>
		/// <param name="article_identifier">Pageid or title of the article to retrieve.</param>
		/// <param name="article">Returned article object.</param>
		/// <returns>True if successful, false if not.</returns>
		override public bool GetArticle(object article_identifier, out Article article)
		{
			db_lock.StartRead();
			try
			{
				article = null;
				using (SQLiteConnection connection = new SQLiteConnection(connection_string))
				{
					connection.Open();
					using (SQLiteCommand command = connection.CreateCommand())
					{
						//prepare statement
						command.CommandText = "SELECT * FROM " + article_table_name + " WHERE pageid == @id OR title == @id";
						command.Parameters.AddWithValue("@id", article_identifier.ToString());

						using (SQLiteDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
								//info for article has been found
								article = new Article(reader.GetInt32(0), reader.GetString(1));
							else
								//article not found
								article = null;
							reader.Close();
						}
					}

					connection.Close();
				}
			}
			catch (Exception e)
			{
#if DEBUG
				Console.WriteLine("Could not retrieve article: " + article_identifier.ToString());
				Console.WriteLine(e.Message);
#endif
				article = null;
			}
			finally
			{
				db_lock.EndRead();
			}
			return article != null;
		}

		/// <summary>
		/// Retrieves the links contained in a wikipedia page with a given pageid.
		/// </summary>
		/// <param name="article">Links contained in this article object will be retrieved</param>
		/// <returns>True if successful, false if not.</returns>
		override public bool LoadLinks(Article article)
		{
			db_lock.StartRead();

			List<int> temp = new List<int>();
			try
			{
				using (SQLiteConnection connection = new SQLiteConnection(connection_string))
				{
					connection.Open();
					using (SQLiteCommand command = connection.CreateCommand())
					{
						command.CommandText = "SELECT destid FROM " + link_table_name + " WHERE sourceid == @pageid";
						command.Parameters.AddWithValue("@pageid", article.PageID);

						using (SQLiteDataReader reader = command.ExecuteReader())
						{
							while (reader.Read()) temp.Add(reader.GetInt32(0));
							reader.Close();
						}
					}
					connection.Close();
				}
				article.Links = temp;
			}
			catch (Exception e)
			{
#if DEBUG
				Console.WriteLine("Could not load links for article: " + article.PageID.ToString());
				Console.WriteLine(e.Message);
#endif
				return false;
			}
			finally
			{
				db_lock.EndRead();
			}

			return true;
		}


		/// <summary>
		/// Retrieves the backlinks found on a wikipedia page of the given Article object.
		/// </summary>
		/// <param name="article">Links contained in this article object will be retrieved.</param>
		/// <returns>True if successful, false if not.</returns>
		override public bool LoadBacklinks(Article article)
		{
			db_lock.StartRead();

			List<int> temp = new List<int>();
			try
			{
				using (SQLiteConnection connection = new SQLiteConnection(connection_string))
				{
					connection.Open();
					using (SQLiteCommand command = connection.CreateCommand())
					{
						command.CommandText = "SELECT sourceid FROM " + backlink_table_name + " WHERE destid == @pageid";
						command.Parameters.AddWithValue("@pageid", article.PageID);

						using (SQLiteDataReader reader = command.ExecuteReader())
						{
							while (reader.Read()) temp.Add(reader.GetInt32(0));
							reader.Close();
						}
					}
					connection.Close();
				}
				article.BackLinks = temp;
			}
			catch (Exception e)
			{
#if DEBUG
				Console.WriteLine("Could not load backlinks for article: " + article.PageID.ToString());
				Console.WriteLine(e.Message);
#endif
				return false;
			}
			finally
			{
				db_lock.EndRead();
			}

			return true;
		}

		/// <summary>
		/// Retrieves the last time the article with the given pageid was revised/touched.
		/// </summary>
		/// <param name="article">Article object to retrieve the link & backlink insert dates.</param>
		/// <returns>Dictionary collection of DateTime the information was inserted, null if unsuccessful.</returns>
		public Dictionary<string, DateTime> GetLastTouchedDates(Article article)
		{
			db_lock.StartRead();

			Dictionary<string, DateTime> dates = null;
			try
			{
				//open connection to database
				using (SQLiteConnection connection = new SQLiteConnection(connection_string))
				{
					connection.Open();
					using (SQLiteCommand command = connection.CreateCommand())
					{
                        command.CommandText = "SELECT linksdate, backlinksdate FROM " + article_table_name +
							" WHERE pageid == @pageid";
						command.Parameters.AddWithValue("@pageid", article.PageID);

						using (SQLiteDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								//info for the article has been found
								dates = new Dictionary<string, DateTime>();

								//if link information is present, parse the date as a DateTime object
								//if not set it to the min value to indicate the information does not exist

								if (reader.IsDBNull(0)) dates.Add("links", DateTime.MinValue);
								else dates.Add("links", DateTime.ParseExact(reader.GetString(0), date_pattern, System.Globalization.CultureInfo.InvariantCulture));

								if (reader.IsDBNull(1)) dates.Add("backlinks", DateTime.MinValue);
								else dates.Add("backlinks", DateTime.ParseExact(reader.GetString(1), date_pattern, System.Globalization.CultureInfo.InvariantCulture));
							}
							reader.Close();
						}
					}
					connection.Close();
				}
			}
			catch (Exception e)
			{
#if DEBUG
				Console.WriteLine("Could not retrieve insert dates for article: " + article.PageID.ToString());
				Console.WriteLine(e.Message);
#endif
				dates = null;
			}
			finally
			{
				db_lock.EndRead();
			}
			return dates;
		}
	}
}
