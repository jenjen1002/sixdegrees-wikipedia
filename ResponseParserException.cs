﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	class ResponseParserException : Exception
	{
		public ResponseParserException() : base()
		{
		}

		public ResponseParserException(string message) : base(message)
		{
		}
		
	}
}
