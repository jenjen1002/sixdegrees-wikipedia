using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SixDegrees
{
	/// <summary>
	/// Static class used to transform a json string into an appropriate collection/container.
	/// (I wrote this class before I was aware that the JavaScriptSerializer exists)
	/// </summary>
	public static class ResponseParser
	{
		//const string key_pattern = "\".*?\"";
		static Regex string_pattern = new Regex("\".*?\"");
		static Regex int_pattern = new Regex("[0-9]+?");

		/// <summary>
		/// Parses a supplied JSON-formatted string into collections of dictionaries and lists.
		/// </summary>
		/// <param name="response">JSON string to parse</param>
		/// <returns>Root node of the resulting dictionary/list collections.</returns>
		public static Dictionary<string, object> Parse(string response)
		{
			List<string> tokens = split_response(response);
			int token_index = 0;

			return parse_dictionary(ref tokens, ref token_index);
		}


		/**Called when a '{' token is encountered in the token list.
		 * Constructs a dictionary object and populates it with keyvalue pairs
		 * found between the initial { and the closing }
		 * Recursive
		 * 
		 **/
		static Dictionary<string, object> parse_dictionary(ref List<string> tokens, ref int i)
		{
			Dictionary<string, object> output = new Dictionary<string, object>();

			string temp_key = null;		//stores the name of a key
			bool complete = false;		//tracks when the dictionary object has finished being populated

			while (!complete)
			{
				//proceed to key token
				++i;

				//expect a valid key token as a string
				if (string_pattern.IsMatch(tokens[i]))
					temp_key = tokens[i].Substring(1, tokens[i].Length - 2);
				else
					throw new ResponseParserException("Invalid token: '" + tokens[i] + "' (token index = " + i + "). Expected a string key of the form \"<key name>\".");

				//proceed to the keyvalue seperator ':'
				++i;

				//expect the keyvalue seperator ':'
				if (tokens[i] != ":")
					throw new ResponseParserException("Invalid token: '" + tokens[i] + "' (token index = " + i + "). Expected a ':' delimiter.");

				//proceed to the value
				++i;

				//expect a string, integer, '{' or '[' token
				if (tokens[i] == "{")
					//the value of this keyvalue pair is a dictionary object, parse a dictionary
					output.Add(temp_key, parse_dictionary(ref tokens, ref i));
				else if (tokens[i] == "[")
					//the value of this keyvalue pair in a list of objects, parse a list
					output.Add(temp_key, parse_list(ref tokens, ref i));
				else if (string_pattern.IsMatch(tokens[i]))
					//the value of this keyvalue pair is a string, exclude the outer quotations and add it to the dictionary
					output.Add(temp_key, tokens[i].Substring(1, tokens[i].Length - 2));
				else if (int_pattern.IsMatch(tokens[i]))
					//the value of this keyvalue pair is an integer, attempt to convert it and add it to the dictionary
					output.Add(temp_key, Convert.ToInt32(tokens[i]));
				else if (tokens[i] == "}")
					//empty container
					break;
				else
					throw new ResponseParserException("Invalid token: '" + tokens[i] + "' (token index = " + i + "). Expected a '{', '[', string, or integer token.");

				//proceed to the next token
				++i;

				//a ',' or a '}' is expected. a ',' indicate another keyvalue pair, a '}' indicates the end of the object
				if (tokens[i] == ",")
					//another keyvalue pair will be entered
					continue;
				else if (tokens[i] == "}")
					complete = true;
				else
					throw new ResponseParserException("Invalid token: '" + tokens[i] + "' (token index = " + i + "). Expected a '}' or ',' token.");

			}

			return output;
		}

		/**Called when a '[' token is encountered.
		 * Constructs a list object and populates it with entries
		 * found in the token list between the '[' and ']'
		 * Recursive
		 * 
		 **/
		static List<object> parse_list(ref List<string> tokens, ref int i)
		{
			List<object> output = new List<object>();

			bool complete = false;

			while (!complete)
			{
				//proceed to first entry
				++i;

				//expect a string, integer, '{' or '[' token
				if (tokens[i] == "{")
					//the value of this entry is a dictionary, parse a new dictionary
					output.Add(parse_dictionary(ref tokens, ref i));
				else if (tokens[i] == "[")
					//the value of this entry is a list, parse a new list
					output.Add(parse_list(ref tokens, ref i));
				else if (string_pattern.IsMatch(tokens[i]))
					//the value of this entry is a string, exclude the quotations and add it to the list
					output.Add(tokens[i].Substring(1, tokens[i].Length - 2));
				else if (int_pattern.IsMatch(tokens[i]))
					//the value of this entry is an integer, attempt to convert it and add it to the list
					output.Add(Convert.ToInt32(tokens[i]));
				else if (tokens[i] == "]")
					//empty container
					break;
				else
					throw new ResponseParserException("Invalid token: '" + tokens[i] + "' (token index = " + i + "). Expected a '{', '[', string, or integer token.");

				//proceed to the next token
				++i;

				//a ',' or a '}' is expected. a ',' indicate another list entry, a '}' indicates the end of the object
				if (tokens[i] == ",")
					//another list entry to be added
					continue;
				else if (tokens[i] == "]")
					complete = true;
				else
					throw new ResponseParserException("Invalid token: '" + tokens[i] + "' (token index = " + i + "). Expected a ']' or ',' token.");

			}

			return output;
		}

		//splits a formatted json string into tokens
		static List<string> split_response(string response)
		{
			int i = 0;
			int j = 0;
			List<string> result = new List<string>();

			while (i < response.Length)
			{
				switch (response[i])
				{
						//delimiters, add them as single character tokens
					case '{':
					case '}':
					case '[':
					case ']':
					case ',':
					case ':':
						result.Add(response[i].ToString());
						break;

						//find closing quote and add whole string as token to the list
					case '"':
						j = closing_quote_index(response, i);
						result.Add(response.Substring(i, j - i + 1));
						i = j;
						break;

						//integer token
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						j = end_of_int(response, i);
						result.Add(response.Substring(i, j - i + 1));
						i = j;
						break;

						//unrecognized token
					default:
						throw new ResponseParserException(String.Format("Unrecognized token {0} starting at index {1}.", response[i].ToString(), i.ToString()));
				};

				++i;
			}

			return result;
		}

		//returns the index of the closing quotation
		//ignores escaped quotations
		static int closing_quote_index(string source, int i)
		{
			try
			{
				int j = i;
                bool escaped = false;
                do
                {
                    ++j;
                    //the escaped status of the current character
                    //the current character is escaped
                    //(preceded by a non-escaped backslash)
                    //if the last character is a backslash, and
                    //the last character (the backslash) was not escaped
                    escaped = (source[j - 1] == '\\') && !escaped;
                }
                while (source[j] != '"' || escaped);
				return j;
			}
			catch (IndexOutOfRangeException)
			{
				throw new ResponseParserException(String.Format("No closing '\"' found in \"{0}\" at index {1}. Reached end of string instead.", source, i.ToString()));
			}
		}

		//returns the index of the last integer-type character
		static int end_of_int(string source, int i)
		{
			int j = i;
			do ++j;
			while (source[j] <= '9' && source[j] >= '0');
			return j - 1;
		}
	}
}

