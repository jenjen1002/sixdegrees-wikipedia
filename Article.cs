﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	/// <summary>
	/// Article class that stores a wikipedia article's pageid, title,
	/// and possibly its links and/or backlinks.
	/// </summary>
	class Article
	{
		public enum InfoType { Links = 0, Backlinks = 1, Weights = 2};

		/// <summary>Allows for easy converting of infotype to a string. </summary>
		public static readonly Dictionary<InfoType, string> TypeToString;

		/// <summary>Static constructor prepares the conversion containers. </summary>
		static Article()
		{
			TypeToString = new Dictionary<InfoType, string>();
			TypeToString.Add(InfoType.Links, "links");
			TypeToString.Add(InfoType.Backlinks, "backlinks");
		}

		/// <summary>Pageid of the wikipedia article. </summary>
		readonly int pageid;
		/// <summary>String title of the wikipedia article. </summary>
		readonly string title;

		/// <summary>A list of pageids indicating what articles this article links to. </summary>
		List<int> links;
		/// <summary>A list of pageids indicating what articles link to this article. </summary>
		List<int> backlinks;

		public Article(int pageid, string title)
		{
			this.pageid = pageid;
			this.title = title;

			links = null;
			backlinks = null;
		}

		public string Title { get { return title;} }
		public int PageID { get { return pageid; } }

		public List<int> Links{
			get { return links; }
			set { links = value; }
		}
		public List<int> BackLinks{
			get { return backlinks; }
			set { backlinks = value; }
		}

		public enum LinkType {Regular = 0, Backlink = 1};

		public List<int> GetLinks(LinkType type)
		{
			if (type == LinkType.Regular) return Links;
			else return BackLinks;
		}

		/// <summary>
		/// Checks the article object for the presence of particular
		/// article information.
		/// </summary>
		/// <param name="type">The information to check for.</param>
		/// <returns>true if the information is present.</returns>
		public bool Contains(Article.InfoType type)
		{
			switch (type)
			{
				case InfoType.Links:
					return links != null;
				case InfoType.Backlinks:
					return backlinks != null;
			}
			return false;
		}

		/// <summary>
		/// Article objects are equal when their pageid's and title's are equal.
		/// Link and weight lists are not compared because one of the objects may only 
		/// be partially loaded.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if (obj == null) return false;

			Article a = obj as Article;
			if (a == null) return false;
			
			return (a.pageid == this.pageid && a.title == this.title);
		}

		public override int GetHashCode()
		{
			return (pageid.ToString() + title).GetHashCode();
		}
	}
}
