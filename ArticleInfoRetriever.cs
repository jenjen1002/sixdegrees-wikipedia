﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	/// <summary>
	/// Responsible for retrieving information related to Article objects.
	/// Retrievers contain api, cache, and database objects and will determine
	/// the appropriate data source to retrieve the information from.
	/// </summary>
	class ArticleInfoRetriever : IDisposable
	{
		const string database_name = "articlecollection";

		/// <summary>
		/// ArticleDatabase shared by all info retrievers to ensure consistent data.
		/// </summary>
		static ArticleDatabase database;
		/// <summary>
		/// ArticleCache shared by all retrievers to ensure only one cache exists at a time.
		/// </summary>
		static ArticleCache article_cache;

		static ArticleInfoRetriever()
		{
            try
            {
                database = new ArticleDatabase(database_name);
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not create/load the article database, working from cache and api only.");
                database = null;
            }
			article_cache = new ArticleCache();
		}



		/// <summary>
		/// ArticleAPI object used to retrieve information from wikipedia's api.
		/// </summary>
		ArticleAPI api;

		/// <summary>
		/// </summary>
		/// <param name="quick_lookup">
		/// If true, dates will be ignored and the api used only
		/// when information is missing from the cache & database.
		/// </param>
		public ArticleInfoRetriever(bool quick_lookup = true)
		{
			QuickLookup = quick_lookup;
			api = new ArticleAPI();
		}

		/// <summary>
		/// If the QuickLookup flag is set the ArticleRetriever object
		/// will only request information from the wikipedia api if there is
		/// no database entry for the desired article. If it is not set the
		/// ArticleInfoManager will compare last-touched dates and determine
		/// if updating the information is necessary/required.
		/// </summary>
		public bool QuickLookup { get; set; }

		/// <summary>
		/// Loads the basic information of an article (pageid and title).
		/// </summary>
		/// <param name="page_identifier">Pageid or title of the article to load.</param>
		/// <returns></returns>
		public bool LoadArticle(object page_identifier, out Article article)
		{
			if (article_cache.GetArticle(page_identifier, out article)) return true;
			if (database.GetArticle(page_identifier, out article)) return true;
			return api.GetArticle(page_identifier, out article);
		}
		
		/// <summary>
		/// Loads article information in to the desired article object.
		/// </summary>
		/// <param name="article">The article to be given the desired information.</param>
		/// <param name="itype">The type of information to retrieve.</param>
		/// <returns>True if successful, false if unsuccessful.</returns>
		public bool LoadInfo(Article article, Article.InfoType itype)
		{
			//attempt to load from the cache
			if (article_cache.Load[itype](article)) return true;

			//does the database contain the information?
			string key_name = Article.TypeToString[itype];
            Dictionary<string, DateTime> last_touched;
            if (database != null)
                last_touched = database.GetLastTouchedDates(article);
            else
                last_touched = null;
			DateTime touched;

			if (last_touched != null)
			{
				//datetime retrieval was successful, check for minvalue to ensure the insert dates are valid
				touched = last_touched[key_name];
				if (touched != DateTime.MinValue)
				{
					//insert dates are valid, use them if quicklookup is set
					if (!QuickLookup)
					{
						//quicklookup is not set, compare to the session start time
						//we assume the information is up to date if it has been inserted during this session
						int compare = touched.CompareTo(ArticleDatabase.SessionStartTime);
						if (compare < 0)
						{
							//insertion occured prior to the session, compare with the date returned by wikipedia's api
							DateTime wiki_date = api.GetLastTouched(article);
							compare = touched.CompareTo(wiki_date);
							if (!(compare < 0))
								//the information is not outdated, load it from the database
								return database.Load[itype](article);
						}
						else
							return database.Load[itype](article);
					}
					else
						return database.Load[itype](article);
				}
			}

			//all else has failed, retrieve via the api
			if (api.Load[itype](article))
			{
				//api retrieval was successful, add the new information to the cache
				article_cache.Add(article, itype);
				return true;
			}

			return false;
		}



		/// <summary>
		/// Attempts to commit any cached article information to the main article database.
		/// </summary>
		public void Dispose()
		{
#if DEBUG
			//Console.WriteLine("Commiting article cache to database.");
#endif
			try
			{
                if (database != null)
                    database.Commit(article_cache.Cache.Values.ToList());

				article_cache.Clear();
				
#if DEBUG
				//Console.WriteLine("Article cache committed successfully to database");
#endif
			}
			catch (Exception e)
			{
#if DEBUG
				Console.WriteLine("Article cache could not be committed.");
				Console.WriteLine(e.Message);
				System.Diagnostics.Debugger.Break();
#endif
			}
			finally
			{ }
		}
	}
}
