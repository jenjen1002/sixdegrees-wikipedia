﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixDegrees
{
	class ConnectionSearchException : Exception
	{
		public ConnectionSearchException()
			: base()
		{
		}

		public ConnectionSearchException(string message)
			: base(message)
		{
		}
	}
}
